module.exports = {
  purge: {
    content: ['./components/**/*.{js,ts,jsx,tsx}', './pages/**/*.{js,ts,jsx,tsx}', './lib/**/*.{js,ts,jsx,tsx}'],
    options: {
      safelist: [/^(md:|lg:)?grid-/, /^(md:|lg:)?col-/, /^(md:|lg:)?gap-/],
    },
  },
  darkMode: 'media', // 'media' or 'class'
  theme: {
    screens: {
      md: '768px',
      lg: '1024px',
      print: {
        raw: 'print',
      },
    },
    colors: {
      cliftonNavy: '#071d49',

      /* Secondary colours */
      millenniumMint: 'blue',
      millenniumMintAccent: 'blue',
      milleniumMintAccentDark: '#00705A',
      anchorBlue: 'blue',
      anchorBlueAccent: '#0857aa',
      harboursideBlue: '#0aabf3',
      collegeRed: '#f55a60',
      collegeRedAccent: '#b11021',
      collegeRedAccentDark: '#9A0E1C',
      spireYellow: '#fee942',
      success: '#F2FAF9',
      warning: '#FBF3F4',

      /* Neutrals */
      black: '#000',
      'grey-800': '#595959',
      'grey-700': '#6d6d6d',
      'grey-600': '#8a8a8a',
      'grey-500': '#c4c4c4',
      'grey-400': '#e7e7e7',
      'grey-300': '#f1f1f1',
      'grey-200': '#f7f7f7',
      'grey-100': '#f8f8f8',
      white: '#fff',
    },
    spacing: {
      0: '0px',
      0.5: '2px',
      1: '4px',
      1.5: '6px',
      2: '8px',
      2.5: '10px',
      3: '12px',
      4: '16px',
      5: '20px',
      6: '24px',
      7: '28px',
      7.5: '30px',
      8: '32px',
      8.5: '34px',
      9: '36px',
      10: '40px',
      10.5: '42px',
      11: '44px',
      12: '48px',
      13: '52px',
      14: '56px',
      15: '60px',
      16: '64px',
      17: '68px',
      18: '72px',
      19: '76px',
      20: '80px',
      21: '84px',
      22: '88px',
      23: '92px',
      24: '96px',
      25: '100px',
      30: '120px',
      37: '148px',
      38: '152px',
      55: '220px',
      150: '600px',
      'container-sm': '15px',
      'container-md': '25px',
      'container-lg': '50px',
    },
    fontFamily: {
      effra: ['Effra', 'Arial', 'Helvetica', 'sans-serif'],
      oswald: ['Oswald', 'Effra', 'Arial', 'Helvetica', 'sans-serif'],
    },
    fontWeight: {
      light: '400',
      regular: '500',
      medium: '600',
      bold: '700',
    },
    fontSize: {
      xxsm: ['12px', { lineHeight: '20px' }], // only to be used on mail icon badge
      xsm: ['14px', { lineHeight: '20px' }],
      sm: ['16px', { lineHeight: '20px' }],
      base: ['19px', { lineHeight: '24px' }], // h5
      lg: ['23px', { lineHeight: '28px' }], // h4
      xl: ['28px', { lineHeight: '32px' }], // h3
      '2xl': ['35px', { lineHeight: '40px' }], // h2
      '3xl': ['50px', { lineHeight: '52px' }], // h1
    },
    lineHeight: {
      none: '1',
      small: '20px',
      normal: '24px',
      lg: '28px',
      xl: '32px',
      '2xl': '40px',
      '3xl': '52px',
    },
    inset: (theme, { negative }) => ({
      auto: 'auto',
      ...theme('spacing'),
      ...negative(theme('spacing')),
    }),
    maxWidth: (theme, { breakpoints }) => ({
      none: 'none',
      0: '0rem',
      xs: '20rem',
      sm: '24rem',
      md: '28rem',
      lg: '32rem',
      xl: '36rem',
      '2xl': '42rem',
      '3xl': '48rem',
      '4xl': '56rem',
      '5xl': '64rem',
      '6xl': '72rem',
      '7xl': '80rem',
      full: '100%',
      min: 'min-content',
      max: 'max-content',
      prose: '65ch',
      container: '1200px',
      ...breakpoints(theme('screens')),
    }),
    fill: (theme) => ({
      ...theme('colors'),
    }),
    extend: {
      transitionDuration: {
        2000: '2000ms',
      },
      outline: {
        focus: ['2px solid #000', '2px'],
      },
      boxShadow: {
        focus: '0px 0px 0px 3px #fff',
        scroll: '0 0 7px 0 rgba(0,0,0,0.13)',
        header: 'rgba(0, 0, 0, 0.15) 0px 0px 16.0001px 0.99997px',
      },
      borderRadius: {
        1.25: '5px',
      },
      borderColor: {
        transparent: 'transparent',
      },
      backgroundColor: {
        transparent: 'transparent',
      },
      backgroundImage: {
        flareMint: 'url(/mint-flare.svg)',
        arrowDown: 'url(/arrow-down.svg)',
        'arrowDown-anchorBlueAccent': 'url(/arrow-down--anchorBlueAccent.svg)',
      },
      backgroundSize: {
        flareSm: '85px 38px',
        flareMd: '156px 70px',
        5: '20px',
      },
      backgroundPosition: {
        'right-3': 'center right 12px',
      },
      opacity: {
        5: '0.05',
      },
      borderWidth: {
        1: '1px',
        5: '5px',
      },
      minWidth: {
        36: '144px',
        64: '256px',
        md: '768px',
        lg: '1024px',
        dropdown: '290px',
        'dropdown-small': '226px',
      },
      zIndex: {
        950: '950',
      },
      keyframes: {
        fadeIn: {
          '0%': { opacity: 0 },
          '100%': { opacity: 1 },
        },
      },
      animation: {
        fadeIn: 'fadeIn 0.5s linear',
      },
    },
  },
  variants: {
    extend: {},
    fill: ['hover', 'focus', 'group-hover'],
    backgroundImage: ['hover', 'focus'],
  },
  plugins: [],
  corePlugins: {
    container: false,
  },
};
