import { createElement, FC, useEffect, useState } from 'react';
import Main from '../layout/Main';
import Metadata from '../layout/Metadata';

const SPINNER_DELAY = 750;

type SpinnerProps = {
  loadingEl?: 'p' | 'h1';
};

const LoadingElement: FC<Required<SpinnerProps>> = ({ loadingEl, children }) => createElement(loadingEl, {}, children);

const Spinner: FC<SpinnerProps> = ({ loadingEl = 'p' }) => {
  const [visible, setVisible] = useState(false);
  useEffect(() => {
    const spinnerTimeout = setTimeout(() => setVisible(true), SPINNER_DELAY);
    return () => clearTimeout(spinnerTimeout);
  }, []);

  return (
    <div className="w-full h-38 flex flex-col items-center justify-center">
      {visible && (
        <>
          <div
            className="w-13 h-13 rounded-full border-2 border-millenniumMint border-t-0 mb-3 animate-spin"
            aria-hidden
          />
          <LoadingElement loadingEl={loadingEl}>Loading...</LoadingElement>
        </>
      )}
    </div>
  );
};

export const PageSpinner = () => (
  <>
    <Metadata />
    <Main>
      <Spinner loadingEl="h1" />
    </Main>
  </>
);

export default Spinner;
