import { createContext, ReactNode, useContext, useState } from 'react';
import { PaginationData } from '@/api/apiSchema';

type PaginationContextProps = {
  pagination: PaginationData;
  setPagination: (pagination: PaginationData) => void;
  selectPage: (page: number) => void;
  resetPagination: () => void;
  data: any[];
  setData: (data: any[]) => void;
};

const DEFAULT_PAGINATION_CONFIG: PaginationData = {
  offset: 0,
  limit: 5,
  total: 0,
};

const DEFAULT_PAGINATION_CONTEXT: PaginationContextProps = {
  pagination: DEFAULT_PAGINATION_CONFIG,
  setPagination: () => undefined,
  selectPage: () => undefined,
  resetPagination: () => undefined,
  data: [],
  setData: () => undefined,
};

const PaginationContext = createContext<PaginationContextProps>(DEFAULT_PAGINATION_CONTEXT);

export const usePaginationContext = () => useContext(PaginationContext);

type PaginationContextProviderProps = {
  limit?: number;
  children: ReactNode;
};

export const PaginationContextProvider = ({
  limit = DEFAULT_PAGINATION_CONFIG.limit,
  children,
}: PaginationContextProviderProps) => {
  const defaultPagination = {
    ...DEFAULT_PAGINATION_CONFIG,
    limit,
  };

  const [pagination, setPagination] = useState(defaultPagination);
  const [data, setData] = useState(DEFAULT_PAGINATION_CONTEXT.data);

  const selectPage = (pageNumber: number) =>
    setPagination({ ...pagination, offset: (pageNumber - 1) * pagination.limit });

  const resetPagination = () => selectPage(1);

  return (
    <PaginationContext.Provider value={{ pagination, selectPage, resetPagination, setPagination, data, setData }}>
      {children}
    </PaginationContext.Provider>
  );
};
