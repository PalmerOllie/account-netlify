import { FC } from 'react';
import { PaginationContextProvider } from './PaginationContext';

const withPaginationProvider =
  <ComponentProps,>(Component: FC<ComponentProps>, limit?: number) =>
  (props: ComponentProps) =>
    (
      <PaginationContextProvider limit={limit}>
        <Component {...props} />
      </PaginationContextProvider>
    );

export default withPaginationProvider;
