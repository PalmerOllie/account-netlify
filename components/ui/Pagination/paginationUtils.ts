import { PaginationData } from '@/api/apiSchema';
import { Breakpoint } from '@/hooks/useBreakpoint';

export type LinkType = number | '...';

export const buildPaginationItems = (currentPage: number, totalPages: number, breakpoint: Breakpoint): LinkType[] => {
  const dotsDisplayLimit = breakpoint === Breakpoint.sm ? 6 : 13;
  const boundaryDisplayLimit = breakpoint === Breakpoint.sm ? 3 : 6;

  let result: LinkType[] = [];
  if (totalPages <= dotsDisplayLimit) {
    result = Array.from({ length: totalPages }, (_, i) => i + 1);
  } else if (currentPage <= boundaryDisplayLimit) {
    const leftBoundaryPages = Array.from({ length: boundaryDisplayLimit + 1 }, (_, i) => i + 1);
    result = [...leftBoundaryPages, '...', totalPages];
  } else if (currentPage <= totalPages - boundaryDisplayLimit) {
    result = [1, '...', currentPage - 1, currentPage, currentPage + 1, '...', totalPages];
  } else {
    const rightBoundaryPages = Array.from(
      { length: boundaryDisplayLimit + 1 },
      (_, i) => totalPages - boundaryDisplayLimit + i,
    );
    result = [1, '...', ...rightBoundaryPages];
  }

  return result;
};

export const translateToPageUnit = ({ offset, limit, total }: PaginationData) => ({
  currentPage: offset / limit + 1,
  totalPages: total % limit === 0 ? total / limit : Math.floor(total / limit) + 1,
});
