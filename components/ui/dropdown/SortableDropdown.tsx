import { useLayoutEffect, useState } from 'react';
import Dropdown, { DropdownItemType } from '@/ui/dropdown/Dropdown';

export type Comparator<T> = (a: T, b: T) => number;
export type SortableDropdownItem<T> = DropdownItemType<Comparator<T>>;

type SortableDropdownProps<T> = {
  classNames?: string;
  id: string;
  label?: string;
  items: SortableDropdownItem<T>[];
  sortMessage: string;
  sortableData: any[];
  defaultComparator?: Comparator<T>;
  onSort: (sortedData: any[]) => void;
};

const notChangeableComparator = () => 0;

const SortableDropdown = <T extends Record<string, unknown>>({
  classNames = '',
  id,
  label = 'Sort by',
  items,
  sortMessage = '',
  sortableData,
  defaultComparator = notChangeableComparator,
  onSort,
}: SortableDropdownProps<T>) => {
  const [message, setMessage] = useState<string>();

  const sortAll = (comparator: Comparator<T>) => {
    const sortedData = [...sortableData].sort(comparator);
    onSort(sortedData);
  };

  const sortOnSelect = (item: DropdownItemType<Comparator<T>>) => {
    sortAll(item.custom || notChangeableComparator);
    setMessage(item.label);
  };

  useLayoutEffect(() => {
    sortAll(defaultComparator);
  }, []);

  return (
    <div className={classNames}>
      <Dropdown id={id} label={label} items={items} onSelect={sortOnSelect} className="md:min-w-dropdown" />
    </div>
  );
};

export default SortableDropdown;
