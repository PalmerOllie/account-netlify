import classNames from 'classnames';
import { Children, createContext, ReactElement, ReactNode, useContext } from 'react';
import { useDelayedBoolean } from '@/hooks/useDelayedBoolean';
import { useScrollIndicator } from '@/hooks/useScrollIndicator';
import RightDoubleArrowIcon from '@/icons/RightDoubleArrowIcon';

type TableContextType = {
  isTableEmpty: boolean;
  headersCount: number;
  noDataMessage?: string | JSX.Element;
};

const TableContext = createContext<TableContextType>({ isTableEmpty: true, headersCount: 0 });

export type TableCellProps = {
  className?: string;
  children?: ReactNode;
  colSpan?: number;
  isHeading?: boolean;
};

export const TableCell = ({ className = 'text-left', colSpan, children }: TableCellProps) => {
  const classes = classNames('text-sm align-top px-4 py-3 md:py-4', {
    [className]: className,
  });
  return (
    <td className={classes} colSpan={colSpan}>
      {children}
    </td>
  );
};

export type TableHeadingCellProps = {
  className?: string;
  children?: ReactNode;
  colSpan?: number;
};

export const TableHeadingCell = ({ className = 'text-left', colSpan, children }: TableHeadingCellProps) => {
  const classes = classNames('text-sm align-top px-4 py-3 md:py-4', {
    [className]: className,
  });
  return (
    <th scope="row" className={classes} colSpan={colSpan}>
      {children}
    </th>
  );
};
export type TableRowProps = {
  className?: string;
  rowIndex?: number;
  children: ReactNode[] | ReactNode;
};

export const TableRow = ({ className = '', children, rowIndex }: TableRowProps) => {
  const trClassName = classNames(className, { 'bg-grey-100': rowIndex !== undefined && rowIndex % 2 === 0 });
  return <tr className={trClassName}>{children}</tr>;
};

type TableFooterProps = {
  children: ReactNode;
};

export const TableFooter = ({ children }: TableFooterProps) => {
  const { isTableEmpty } = useContext(TableContext);
  return isTableEmpty ? null : <tfoot className="border-b-1 border-grey-500">{children}</tfoot>;
};

export type TableBodyProps = {
  children?: ReactNode;
};

export const TableBody = ({ children }: TableBodyProps) => {
  const { isTableEmpty, headersCount, noDataMessage } = useContext(TableContext);
  return (
    <tbody className="border-b-1 border-grey-500">
      {isTableEmpty ? (
        <TableRow>
          <TableCell className="text-left md:text-center" colSpan={headersCount}>
            {noDataMessage || 'No data available to be displayed'}
          </TableCell>
        </TableRow>
      ) : (
        children
      )}
    </tbody>
  );
};

export type TableHeaderData = {
  value: string;
  className?: string;
};

export type TableHeaderProps = {
  headers: TableHeaderData[];
};

export const TableHeader = ({ headers }: TableHeaderProps) => {
  const { isTableEmpty } = useContext(TableContext);
  return (
    <thead className="border-t-1 border-black" aria-hidden={isTableEmpty}>
      <tr>
        {headers.map(({ className, value }) => {
          const thClasses = classNames(
            'text-sm font-medium px-4 py-3 md:py-4 text-left border-b-1 border-grey-500 whitespace-nowrap',
            className,
          );
          return (
            <th className={thClasses} key={value}>
              {value}
            </th>
          );
        })}
      </tr>
    </thead>
  );
};

export type TableProps = {
  caption?: string;
  noDataMessage?: string | JSX.Element;
  className?: string;
  disableScroll?: boolean;
  children:
    | [ReactElement<TableHeaderProps, typeof TableHeader>, ReactElement<TableBodyProps, typeof TableBody>]
    | [
        ReactElement<TableHeaderProps, typeof TableHeader>,
        ReactElement<TableBodyProps, typeof TableBody>,
        ReactElement<TableFooterProps, typeof TableFooter> | null,
      ];
};

const Table = ({ caption, noDataMessage, className, disableScroll, children }: TableProps) => {
  const [tableHeader, tableBody, tableFooter] = children;

  const headersCount = tableHeader.props.headers.length;
  const rowsCount = Children.toArray(tableBody.props.children).length;
  const isTableEmpty = rowsCount < 1;

  const { ref, showScroll } = useScrollIndicator();
  const scrollInDom = useDelayedBoolean({ value: showScroll, delay: { delayFalse: 2000, delayTrue: 0 } });

  const scrollIndicatorClasses = classNames(
    'inline-flex items-center lg:hidden',
    'bg-spireYellow text-xsm',
    'absolute right-0 top-8.5 md:top-10.5',
    'overflow-hidden',
    { 'transition-all duration-2000 opacity-0': !showScroll },
  );

  const scrollContainerClasses = classNames({
    'overflow-x-auto': !disableScroll,
    'overflow-hidden': disableScroll,
  });

  return (
    <TableContext.Provider value={{ isTableEmpty, headersCount, noDataMessage }}>
      <div className="relative overflow-hidden">
        <div className={scrollContainerClasses} ref={ref}>
          {scrollInDom && (
            <p className={scrollIndicatorClasses} aria-hidden>
              <span className="ml-1 mr-1">Scroll</span> <RightDoubleArrowIcon width={12} height={12} className="mr-3" />
            </p>
          )}
          <table className={className}>
            {caption && <caption>{caption}</caption>}
            {tableHeader}
            {tableBody}
            {tableFooter}
          </table>
        </div>
      </div>
    </TableContext.Provider>
  );
};

export default Table;
