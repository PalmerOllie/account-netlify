import { ReactNode } from 'react';

type MainProps = {
  children: ReactNode;
};

const Main = ({ children }: MainProps) => <main id="mainContent">{children}</main>;

export default Main;
