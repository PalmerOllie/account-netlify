import classNames from 'classnames';
import { ReactNode } from 'react';

type GridProps = {
  sm?: number;
  md?: number;
  lg?: number;
  smStart?: number;
  mdStart?: number;
  lgStart?: number;
  className?: string;
  children: ReactNode;
};

const GridSpan = ({ sm, md, lg, smStart, mdStart, lgStart, className, children }: GridProps) => {
  const classes = classNames(
    { [`col-span-${sm}`]: sm },
    { [`md:col-span-${md}`]: md },
    { [`lg:col-span-${lg}`]: lg },
    { [`col-start-${smStart}`]: smStart },
    { [`md:col-start-${mdStart}`]: mdStart },
    { [`lg:col-start-${lgStart}`]: lgStart },
    className,
  );

  return <div className={`${classes}`}>{children}</div>;
};

export default GridSpan;
