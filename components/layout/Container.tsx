import classNames from 'classnames';
import { ReactNode } from 'react';

/**
 * accepts 'left' and 'right' props for medium and large.
 * mobile screen has always got left and right padding.
 */

type ContainerProps = {
  left?: boolean;
  right?: boolean;
  className?: string;
  children: ReactNode;
};

const Container = ({ left = false, right = false, children, className }: ContainerProps) => {
  const classes = classNames(
    { 'px-container-sm md:px-container-md lg:px-container-lg': left && right },
    { 'px-container-sm md:pl-0 md:pr-container-md lg:pr-container-lg': !left && right },
    { 'px-container-sm md:pr-0 md:pl-container-md lg:pl-container-lg': left && !right },
    { 'px-container-sm md:px-0 lg:px-0': !left && !right },
    className,
  );
  return <div className={classes}>{children}</div>;
};

export default Container;
