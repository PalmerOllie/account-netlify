import { ReactNode } from 'react';
import Main from './Main';

type PageWrapperProps = {
  children: ReactNode;
};

const PageWrapper = ({ children }: PageWrapperProps) => (
  <div className="mx-auto max-w-container mb-20">
    <Main>{children}</Main>
  </div>
);

export default PageWrapper;
