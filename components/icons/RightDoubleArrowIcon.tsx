import SVG, { SVGProps } from '@/icons/SVG';

const RightDoubleArrowIcon = (props: SVGProps) => (
  <SVG {...props}>
    <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g transform="translate(-2.000000, 0.000000)" fill="#000000">
        <g transform="translate(4.000000, 0.000000)">
          <g transform="translate(6.000000, 6.000000) scale(-1, -1) rotate(90.000000) translate(-6.000000, -6.000000) ">
            <polygon points="11.2264521 3.39854793 5.97645207 8.60145207 0.726452066 3.39854793 2.56319092 3.39854793 5.90604612 6.71809306 5.97645207 6.788499 9.39018893 3.39854793" />
          </g>
        </g>
        <g transform="translate(6.000000, 6.000000) scale(-1, -1) rotate(90.000000) translate(-6.000000, -6.000000) ">
          <polygon points="11.2264521 3.39854793 5.97645207 8.60145207 0.726452066 3.39854793 2.56319092 3.39854793 5.90604612 6.71809306 5.97645207 6.788499 9.39018893 3.39854793" />
        </g>
      </g>
    </g>
  </SVG>
);

export default RightDoubleArrowIcon;
