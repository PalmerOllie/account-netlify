import { getPreviousInvestments } from '../lib/handlers/previousInvestmentsHandler';

export const handler = (event: any) => {
  const { clientNo } = event.params.path;
  const { sortBy, offset, limit } = event.params.querystring;
  return getPreviousInvestments(clientNo, sortBy, offset, limit);
};
