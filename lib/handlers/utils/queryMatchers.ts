import { firstDayOfTaxYear } from '@/utils/firstDayOfYear';

export type GetProp<T> = (obj: T) => string;

export const matchByPropValue =
  <T>(type: any, getType: GetProp<T>) =>
  (obj: T) =>
    !type || getType(obj) === type;

export const matchByTimePeriod =
  <T>(period: any, periodUnit: any, getDate: GetProp<T>) =>
  (obj: T) => {
    if (typeof period !== 'string' || typeof periodUnit !== 'string') {
      return true;
    }

    const periodNumber = parseInt(period, 10);
    const currentDate = new Date();
    const objDate = new Date(getDate(obj));

    switch (periodUnit) {
      case 'day': {
        const periodDateTime = new Date().setDate(currentDate.getDate() - periodNumber);
        return periodDateTime <= objDate.getTime();
      }
      case 'month': {
        const periodDateTime = new Date().setMonth(currentDate.getMonth() - periodNumber);
        return periodDateTime <= objDate.getTime();
      }
      case 'taxYear': {
        return (
          objDate.getTime() >= firstDayOfTaxYear(periodNumber).getTime() &&
          objDate.getTime() < firstDayOfTaxYear(periodNumber + 1).getTime()
        );
      }
      default:
        return true;
    }
  };
