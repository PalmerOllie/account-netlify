import { useRouter } from 'next/router';
import { createContext, ReactNode, useEffect, useState } from 'react';

export type GlobalContextProp = {
  routeChanged: boolean;
};

export const GlobalContext = createContext<GlobalContextProp>({ routeChanged: false });

type AppContextProps = {
  children: ReactNode;
};

const AppContext = ({ children }: AppContextProps) => {
  const router = useRouter();
  const [globalState, setGlobalState] = useState({ routeChanged: false });

  useEffect(() => {
    const onRouteChangeStart = () => {
      if (!globalState.routeChanged) setGlobalState({ ...globalState, routeChanged: true });
    };
    router.events.on('routeChangeStart', onRouteChangeStart);
    return () => {
      router.events.off('routeChangeStart', onRouteChangeStart);
    };
  }, []);

  return <GlobalContext.Provider value={globalState}>{children}</GlobalContext.Provider>;
};

export default AppContext;
