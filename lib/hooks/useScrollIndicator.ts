import { useEffect, useRef, useState } from 'react';
import { useIntersectionObserver } from '@/hooks/useIntersectionObserver';
import { useWindowWidth } from '@/hooks/useWindowWidth';

export const useScrollIndicator = () => {
  const ref = useRef<HTMLDivElement>(null);
  const intersecting = useIntersectionObserver(ref);
  const windowsWidth = useWindowWidth();
  const [scrolledAtLeastOnce, setScrolledAtLeastOnce] = useState(false);
  const [showScroll, setShowScroll] = useState(false);

  useEffect(() => {
    const scrollHandler = () => {
      setScrolledAtLeastOnce(true);
    };
    if (!scrolledAtLeastOnce) {
      ref.current?.addEventListener('scroll', scrollHandler);
    }

    return () => {
      ref.current?.removeEventListener('scroll', scrollHandler);
    };
  }, [scrolledAtLeastOnce]);

  useEffect(() => {
    const scrollWidth = ref.current?.scrollWidth || 0;
    const clientWidth = ref.current?.clientWidth || 0;
    setShowScroll(!scrolledAtLeastOnce && scrollWidth > clientWidth);
  }, [windowsWidth, scrolledAtLeastOnce, intersecting]);

  return { ref, showScroll };
};
