import { RefObject, useEffect, useState } from 'react';

export const useIntersectionObserver = (ref: RefObject<HTMLElement>) => {
  const [intersecting, setIntersecting] = useState(false);

  useEffect(() => {
    const observer = new IntersectionObserver((entries) => {
      setIntersecting(entries?.[0]?.isIntersecting || false);
    });

    if (ref.current) {
      observer.observe(ref.current);
    }

    return () => {
      observer.disconnect();
    };
  }, []);

  return intersecting;
};
