export const removeSpaces = (str: string) => str.split(' ').join('');
