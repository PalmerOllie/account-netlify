export const byStringAsc = (a: string, b: string) => a.localeCompare(b);
export const byStringDesc = (a: string, b: string) => b.localeCompare(a);
export const byNumberAsc = (a: number | string, b: number | string) => Number(a) - Number(b);
export const byNumberDesc = (a: number | string, b: number | string) => Number(b) - Number(a);
export const byDateAsc = (a: string, b: string) => Date.parse(a) - Date.parse(b);
export const byDateDesc = (a: string, b: string) => Date.parse(b) - Date.parse(a);
