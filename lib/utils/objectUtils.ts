export const prop = (property?: string | number, object?: any) =>
  typeof property !== 'undefined' && object && Object.hasOwnProperty.call(object, property)
    ? object[`${property}`]
    : undefined;
