export const formatDate = (date: string | Date, options: Intl.DateTimeFormatOptions = {}) =>
  (typeof date === 'string' ? new Date(date) : date).toLocaleDateString('en-GB', options);

const formatTime = (date: string | Date) =>
  (typeof date === 'string' ? new Date(date) : date).toLocaleTimeString('en-GB', {
    hour: '2-digit',
    minute: '2-digit',
  });

export const formatDateTime = (datetime: string | Date) => `${formatDate(datetime)} ${formatTime(datetime)}`;
