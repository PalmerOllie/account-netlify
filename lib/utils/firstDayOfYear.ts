export const firstDayOfCalendarYear = (year: number) => new Date(year, 0, 1);
export const firstDayOfTaxYear = (year: number) => new Date(year, 3, 6);
